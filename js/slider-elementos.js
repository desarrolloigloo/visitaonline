$(document).ready(function(){
    var angle = 0;
    
    $("#flecha-izq").unbind().click(function(){
       movimientoFlecha('ant','.modulo','.tira-slides');
    });
    $("#flecha-der").unbind().click(function(){
        movimientoFlecha('prox','.modulo','.tira-slides');
    });
    $("#flecha-izq-re").unbind().click(function(){
       movimientoFlechaRedenciones('ant','.premio','.tira-slides-re');
    });
    $("#flecha-der-re").unbind().click(function(){
        movimientoFlechaRedenciones('prox','.premio','.tira-slides-re');
    });
$(window).load(function()
   { 
       ajustarTira();
       ajustarTiraRedenciones();
   });
});

function ajustarTira(){
    var anchoElem = $('.modulo').outerWidth(true);
    var cantElem  = $('.modulo').length;
    var anchoTira = cantElem * anchoElem;
    console.log(anchoElem + '-' )
    $('.tira-slides').css('width',anchoTira + 100 + 'px');
     
     if(window.innerWidth > 852)
    { 
    if(cantElem <= 4)
    {
        $('.flecha-pe').css('visibility','hidden');
    }else
    {
        $('.flecha-pe').css('visibility','visible');
    }
    }
    
}
function ajustarTiraRedenciones(){
    var anchoElem = $('.premio').outerWidth(true);
    var cantElem  = $('.premio').length;
    
    /*var mod = cantElem % 5;
    var anchoTira = cantElem * anchoElem;
    if(mod==0 || cantElem < 6)
    {
        cantElem  = $('.premio').length;   
    }else
    {
        var div= (cantElem/5);
        var ajuste = div + 1
        cantElem = ajuste * 5
    }*/
    var anchoTira = cantElem * anchoElem;
    console.log(cantElem + '-' )
    $('.tira-slides-re').css('width',anchoTira + 200 + 'px');
    
    if(window.innerWidth > 852)
    {
        if(cantElem <= 6)
        {
            $('.flecha-re').css('visibility','hidden');
        }else
        {
            $('.flecha-re').css('visibility','visible');
        }
    }
}

function movimientoFlecha(direccion,elemento,tira)
{
    var anchoElem = parseInt($(elemento).css('width'));
    var limDer    = parseInt($(elemento).css('margin-right'));
    var cantElem  = $(elemento).length;
    var anchoTira = $('.tira-slides').width();//(cantElem * anchoElem) + (cantElem * limDer);
    
    //var sobrante  = anchoElem * 5 + anchoTira * 5;
    var seccionTira = $(elemento).outerWidth(true) * 4 + 14;
    var marginLeftTira =parseInt($(tira).css('margin-left'));
    console.log('seccionT:' + seccionTira)
    //$(tira).css('width',anchoTira + 'px');
   
        /*for(i=0;i<=cantElem;i+=4)
        {
            var ultElem = elemento + ':nth-child(' + i + ')';
            
            $(ultElem).css('margin-right',0)
        }*/
        
        if(direccion == 'prox' && marginLeftTira > - anchoTira + seccionTira + 40)
        {
            $(tira).animate({marginLeft: "-="+ seccionTira}, 1000);
            console.log('prox')
        }else
        {
            if(direccion == 'ant' && marginLeftTira < -1)
            {
                $(tira).animate({marginLeft: "+="+ seccionTira}, 1000);
            }
        }
}
function movimientoFlechaRedenciones(direccion,elemento,tira)
{
    var anchoElem = parseInt($(elemento).css('width'));
    var limDer    = parseInt($(elemento).css('margin-right'));
    var cantElem  = $(elemento).length;
    var anchoTira =  $('.tira-slides-re').width();//(cantElem * anchoElem) + (cantElem * limDer);
    
    //var sobrante  = anchoElem * 6 + anchoTira * 6;
    var seccionTira = $(elemento).outerWidth(true) * 5 + 26;
    var marginLeftTira =parseInt($(tira).css('margin-left'));
    console.log('seccionT:' + seccionTira)
    //$(tira).css('width',anchoTira + 'px');
   
        /*for(i=0;i<=cantElem;i+=4)
        {
            var ultElem = elemento + ':nth-child(' + i + ')';
            
            $(ultElem).css('margin-right',0)
        }*/
        
        if(direccion == 'prox' && marginLeftTira > - anchoTira + seccionTira)
        {
            $(tira).animate({marginLeft: "-="+ seccionTira}, 1000);
            console.log('prox')
        }else
        {
            if(direccion == 'ant' && marginLeftTira < -1)
            {
                $(tira).animate({marginLeft: "+="+ seccionTira}, 1000);
            }
        }
}

/*function minimizarZ()
{
    var z= -815;
    
    
    
}*/
function galleryspin(sign) {
	spinner = $("#spinner");
	if (!sign) {
		angle = angle + 45;
	} else {
		angle = angle - 45;
	}
	spinner.setAttribute("style","-webkit-transform: rotateY("+ angle +"deg);transform: rotateY("+ angle +"deg);");
}