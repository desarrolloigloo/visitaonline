$(document).ready(function() {

    $(window).load(function() {
        asignarLogo();
        acomodarElementos();
        agregarBotonLat();
    });

});

function acomodarElementos() {
    var altoWin = window.innerHeight;
    var altoMenuSup = $('header').height();
    var altoOpcLat = $('#menu-lateral ul').height();
    var altoLat = altoWin - altoMenuSup;
    var anchoWin = window.innerWidth;
    var anchoMenuLat = $('#menu-lateral').outerWidth() + 5;
    var anchoCont = anchoWin - anchoMenuLat;

    if (anchoWin < 852) {
        $('#menu-lateral').css('height', altoWin + 'px');
        $('.contenido-principal').css('height', altoWin + 'px');
    }
    else {
        if (altoOpcLat <= altoLat) {
            $('#menu-lateral').css('height', altoLat + 'px');
            $('.contenido-principal').css('height', altoLat + 'px');
        }
        else {
            $('#menu-lateral').css('height', altoOpcLat + 'px');
            $('.contenido-principal').css('height', altoOpcLat + 'px');
        }
    }
}

function asignarLogo() {
    if (window.innerWidth < 852) {
        $('#img-logo').attr('src', 'img/icon/logo-movil-mk.png');
    }
    else {
        $('#img-logo').attr('src', 'img/icon/logo-visita-mk.png');
    }
    console.log('hola');
}

function agregarBotonLat() {
    if (window.innerWidth < 852) {
        $('<li><a class="opcion-lat" href="#"><img src="img/icon/tq-opc.png">PÁGINA <br> PRINCIPAL</a></li>').insertBefore("#menu-lateral ul li:last-child");
    }
    else {
        $('<li><a class="opcion-lat" href="#"><img src="img/icon/tq-opc.png">PÁGINA <br> PRINCIPAL</a></li>').remove();
    }
}

function mostrarVisitador() {
    $('#visitador').fadeIn(500);
}

function mostrarVisitadorTip() {
    $('#visitador-tip').fadeIn(500);
}

function ocultarVisitador() {
    var cantVid = $('.card').length;

    $('#visitador').fadeOut(500);

    //if (cantVid == 2) {
    //    console.log('Dos');
        if (window.innerWidth < 852) {
            $('#videos').animate({
                right: "26%",
                width: "100%",
               
            }, 0, function() {
                /*$('#uno:checked ~ .una .otra').css('display','block');
                $('#prox:checked ~ .dos .otra').css('display','block');*/
            });
            $('#videos').css('display','block');
        }
        else {
            /*$('#videos').animate({
                right: "26%",
                width: "45%"*/
            $('#videos').addClass('anima-centro', 500);
            
            /*}, 500, function() {
                /*$('#uno:checked ~ .una .otra').css('display','block');
                $('#prox:checked ~ .dos .otra').css('display','block');*/
            }//);
}

function ocultarVisitadorTip() {
    var cantVid = $('.card').length;

    $('#visitador-tip').fadeOut(500);
    if(window.innerWidth > 852)
    {
        $('.info-izq').animate({
            marginLeft: "19%",
            width: '65%'
    
        }, 500, function() {
            /*$('#uno:checked ~ .una .otra').css('display','block');
            $('#prox:checked ~ .dos .otra').css('display','block');*/
        });
    }else
    {
        $('.info-izq').css('display','block')
    }
}

function ocultarPro() {
    window.location.href = "index.html";
    $("#visitador #video").attr("src", 'https://player.vimeo.com/external/147785362.hd.mp4?s=66fa7254d4ba562ac9e673bb6a6b3f6ea538c35f&profile_id=113');
}
