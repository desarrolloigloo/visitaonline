
    function showDialog(){
        $("#pregunta-video").show();
        //$(".coverPopup").show();
    }
    function closeDialog() { 
        $("#pregunta-video").hide();
    } 
    
    function showDialogCorrecta(){
        $("#respuesta-correcta").show();
    }
    function closeCorrecta() { 
        $("#respuesta-correcta").hide();
    }
    function showDialogIncorrecta(){
        $("#respuesta-incorrecta").show();
    }
    function closeIncorrecta() { 
        $("#respuesta-incorrecta").hide();
    }
    function showDialogCalifica(){
        $("#calificar-video").show();
    }
    function closeDialogCalifica() { 
        $("#calificar-video").hide();
    }
    function showDialogMuestra(){
        $("#confirma-muestra").show();
    }
    function closeMuestra() { 
        $("#confirma-muestra").hide();
    }
    function showDialogSorteo(){
        $("#confirma-sorteo").show();
    }
    function closeSorteo() { 
        $("#confirma-sorteo").hide();
    }
    function showDialogDirecta(){
        $("#confirma-directa").show();
    }
    function closeDirecta() { 
        $("#confirma-directa").hide();
    } 
    function showDialogAyuda(){
        $("#info-ayuda").show();
    }
    function closeAyuda() { 
        $("#info-ayuda").hide();
    } 
    function showDialogHistoRedenciones(){
        $("#historial-redenciones").show();
    }
    function closeHistoRedenciones() { 
        $("#historial-redenciones").hide();
    }
    function showDialogVideo(elemt){
        elem = '#' + elemt;
        if($(elem).hasClass('rondell-item-focused'))
        {
        $(".contenido-video-produc").show();
        }
    }
    function closeVideo() { 
        $(".contenido-video-produc").hide();
    }
    function showDialogCarga(){
        $(".carga-web").show();
    }