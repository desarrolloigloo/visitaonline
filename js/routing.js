var app = angular.module('visita-online', ['ngRoute']);

app.config(function($routeProvider,$locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'vistas/modulo.html'
        })
        .when('/modulo', {
            templateUrl: 'vistas/modulo.html'
        })
        .when('/perfil', {
            templateUrl: 'vistas/perfil.html'
        })
        .when('/muestra-medica', {
            templateUrl: 'vistas/muestra.html'
        })
        .when('/actividades-academicas', {
            templateUrl: 'vistas/actividades.html',
        })
        .when('/instructivo', {
            templateUrl: 'vistas/instructivo.html',
        })
        .when('/historial-redenciones', {
            templateUrl: 'vistas/historial-redenciones.html',
        })
        .when('/info', {
            templateUrl: 'vistas/info.html',
        })
        .when('/video', {
            templateUrl: 'vistas/video-pro.html',
        })
        
        
});


app.directive( 'goClick', function ( $location ) {
  return function ( scope, element, attrs ) {
    var path;

    attrs.$observe( 'goClick', function (val) {
      path = val;
    });

    element.bind( 'click', function () {
      scope.$apply( function () {
        $location.path( path );
      });
    });
  };
});
